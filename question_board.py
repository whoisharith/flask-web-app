'''Contains references to app components and variables for flask shell'''
from app import app_flask, db
from app.models import User, Question, Answer, Tag, QuestionTag

@app_flask.shell_context_processor
def make_shell_context():
    '''Add the imported objects and classes to shell context'''
    return {
        'db': db,
        'User': User,
        'Question': Question,
        'Answer': Answer,
        'Tag': Tag,
        'QuestionTag': QuestionTag
    }

if __name__ == '__main__':
    app_flask.run()

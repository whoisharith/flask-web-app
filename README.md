This repo contains the python files for the back-end of a flask project

(a stack-overflow clone with limited features.)

Features of the web-app:

    >Register user
    >Login as a user
    >Log a user out               (restricted to logged in users)
    >Change own password                    ...
    >Delete own account                     ...

    >Browse all questions, answers, tags, users
    
    >Post a question              (restricted to logged in users)
    >Post an answer                         ...    
    >Vote on a question/answer              ...
    
    >Edit a question/answer       (restricted to original author)
    >Delete a question/answer               ...

    The web-app only includes a back-end server at this point of time. Use
    Postman or any other API monitoring tool to send requests and receive
    responses in JSON form.

    Future releases may include an HTML/CSS/JS user interface.

Technologies Used:

    Python
    Flask
    SQLAlchemy
    SQLite
    Postman API tool

Instructions:

    1) Install python 3.7.

    2) Clone/download the project files, without modifying the internal
    directory structure.

    3) Run this bash command in the project root directory to install all
    dependencies:

        pip install -r requirements.txt

    4) Install Postman or any other API monitoring tool.

    5) Read documentation.html for information on API call structure.
    
    6) Run this bash command in the project root directory to start the server:

        flask run

    7) Optional: Import collection_question_board_part_i.json in Postman if
    you're lazy like me.

    8) Send your API calls.


'''Initialise the app and dependency modules.'''

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_restful import Api
from flask_jwt_extended import JWTManager

from config import Config

app_flask = Flask(__name__)
app_flask.config.from_object(Config)
app_flask.config.from_envvar('ENV_FILE_LOCATION')

db = SQLAlchemy(app_flask)
migrate = Migrate(app_flask, db)
bcrypt = Bcrypt(app_flask)
jwt = JWTManager(app_flask)

blacklist = set()
error_log_file = 'errors.log'

from app import routes, models

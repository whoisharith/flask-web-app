'''Contains ORM classes to map objects to database rows and vice-versa.
'''

from datetime import datetime

from flask_bcrypt import generate_password_hash, check_password_hash

from app import db

class DataStructure():
    '''Base class which contains a method to convert a table row
    to JSON form.'''
    def convert_to_json_form(self):
        '''Convert a row object to JSON form.\n
        :return json_form : dict, with columns as keys and entries as values.
        '''
        columns = self.__table__.columns.keys()
        json_form = {}
        for col in columns:
            col_value = self.__getattribute__(col)
            if isinstance(col_value, datetime):
                col_value = col_value.strftime("%Y/%m/%d, %H:%M:%S") + " UTC"
            json_form[col] = col_value
        return json_form

class User(db.Model, DataStructure):
    '''User class used to map a user object to a database table row.
    Inherits from the SQLAlchemy.Model base class.
    '''
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password = db.Column(db.String(120))
    is_active = db.Column(db.Boolean, default=True)

    questions = db.relationship('Question', backref='author', lazy='dynamic')
    answers = db.relationship('Answer', backref='author', lazy='dynamic')

    def __repr__(self):
        '''Return a string representation of the user object when called.'''
        return f'<User {self.username}>'

    def hash_password(self):
        '''Hash a password upon registration, and store as an instance variable
        password. The library flask_bcrypt is used to hash the password.
        '''
        self.password = generate_password_hash(self.password).decode('utf8')

    def check_password(self, password):
        '''On login, check if the stored hash matches with the hash of the
        entered password.\n
        :return True or False
        '''
        return check_password_hash(self.password, password)

class Question(db.Model, DataStructure):
    '''Question class used to map a question object to a database table row.\n
    Inherits from SQLAlchemy.Model and DataStructure classes.\n
    Overrides to_JSON_form method to adapt to the question row object.
    '''
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    description = db.Column(db.String(300))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    score = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    answers = db.relationship('Answer', backref='question', lazy='dynamic')
    qtags = db.relationship('QuestionTag', backref='question', lazy='dynamic')

    def __repr__(self):
        '''Return a string representation of the question object when called.'''
        return f'<Question {self.title}>'

    def convert_to_json_form(self):
        '''Override the method from DataStructure class, to add fields answers,
        tags and authors.
        :return json_form : dict, columns as keys and entries as values.
        '''
        columns = self.__table__.columns.keys()
        json_form = {}
        for col in columns:
            col_value = self.__getattribute__(col)
            if isinstance(col_value, datetime):
                col_value = col_value.strftime("%Y/%m/%d, %H:%M:%S") + " UTC"
            json_form[col] = col_value
        json_form['answers'] = [answer.convert_to_json_form()\
            for answer in self.answers]
        json_form['tags'] = [qtag.tag.name for qtag in self.qtags]
        json_form['author'] = self.author.username
        if not self.author.is_active:
            json_form['author'] = '[deleted]'
            json_form['user_id'] = '[deleted]'
        return json_form

class Answer(db.Model, DataStructure):
    '''Answer class used to map a answer object to a database table row.\n
    Inherits from SQLAlchemy.Model and DataStructure classes.\n
    Overrides convert_to_json_form method to adapt to the answer row object.
    '''
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(500))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    score = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))

    def __repr__(self):
        '''Return a string representation of the answer object when called.'''
        return f'<Answer {self.body}>'

    def convert_to_json_form(self):
        '''Override the method from DataStructure class to add the author field.
        \n
        :return json_form : dict, columns as keys and entries as values.
        '''
        columns = self.__table__.columns.keys()
        json_form = {}
        for col in columns:
            value = self.__getattribute__(col)
            if isinstance(value, datetime):
                value = value.strftime("%Y/%m/%d, %H:%M:%S") + " UTC"
            json_form[col] = value
        json_form['author'] = self.author.username
        if not self.author.is_active:
            json_form['author'] = '[deleted]'
            json_form['user_id'] = '[deleted]'
        return json_form

class Tag(db.Model, DataStructure):
    '''Tag class used to map a tag object to a database table row.\n
    Inherits from SQLAlchemy.Model and DataStructure classes.
    '''
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True)
    qtags = db.relationship('QuestionTag', backref='tag', lazy='dynamic')

    def __repr__(self):
        '''Return a string representation of the tag object when called.'''
        return f'<Tag {self.name}>'

class QuestionTag(db.Model, DataStructure):
    '''Tag class used to map a question tag object to a database table row.\n
    Inherits from SQLAlchemy.Model and DataStructure classes.
    '''
    id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    tag_id = db.Column(db.Integer, db.ForeignKey('tag.id'))

    def __repr__(self):
        '''Return a string representation of the tag object when called.'''
        return f'<QTag {self.tag.name}>'

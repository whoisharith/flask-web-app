'''Includes all functions which handle different requests based on the request
URL.
'''

import datetime
import werkzeug

from flask import redirect, request, jsonify
from flask_jwt_extended\
    import create_access_token, jwt_required, get_jwt_identity, get_raw_jwt

from app import app_flask, db, jwt, blacklist, error_log_file
from app.models import User, Question, Answer, Tag
from app.helpers import englify, find_tags_for, add_tags_to_db, save_error_log

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    '''Checks whether a jwt token has been blacklisted or not.\n
    :param decrypted_token : str, token\n
    :return jti in blacklist : boolean
    '''
    jti = decrypted_token['jti']
    return jti in blacklist

@app_flask.route('/signup/', methods=['POST'])
@app_flask.route('/signup', methods=['POST'])
def register_user():
    '''Handles POST request to register a new user.\n
    User details are received as request body.\n
    :return dict, with id and username of created user.
    '''
    try:
        user = User(**request.get_json())
        user.hash_password()
        clean_username = englify(user.username)
        already_exists = User.query.filter_by(username=clean_username).first()\
            or User.query.filter_by(email=user.email).first()
        if len(clean_username) < 6 or clean_username != user.username:
            return {"error": "Username invalid."}, 400
        if not already_exists:
            user.username = clean_username
            db.session.add(user)
            db.session.commit()
            return {"id": user.id, "username": user.username}
        return {"error": "Username/email already exists."}, 409
    except (KeyError, werkzeug.exceptions.BadRequest):
        db.session.rollback()
        return {"error": "Bad Request"}, 400
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/account/password/', methods=['PUT'])
@app_flask.route('/account/password', methods=['PUT'])
@jwt_required
def change_password():
    '''Handle PUT request to update a user's password.'''
    try:
        user = User.query.filter_by(username=get_jwt_identity()).first()
        user.password = request.get_json['password']
        user.hash_password()
        db.session.commit()
        return {"message": "Successfully changed password."}
    except (KeyError, werkzeug.exceptions.BadRequest):
        return {"error": "Bad Request"}, 400
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/login/', methods=['POST'])
@app_flask.route('/login', methods=['POST'])
def login_user():
    '''Handles POST request to login as a user and issue JW token.\n
    :return dict with access token which is valid for 7 days.
    '''
    try:
        request_body = request.get_json()
        user = User.query.filter_by(username=request_body['username']).first()
        if user is None or not user.is_active:
            return {"error": "No such user."}, 404
        authorised = user.check_password(request_body['password'])
        if not authorised:
            return {'error': 'Password is wrong.'}, 401
        expiry = datetime.timedelta(days=7)
        access_token = create_access_token(identity=str(user.username), \
            expires_delta=expiry)
        return {'token': access_token}, 200
    except (KeyError, werkzeug.exceptions.BadRequest):
        return {"error": "Bad Request"}, 400
    except:
        save_error_log(error_log_file)
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/logout/', methods=['DELETE'])
@app_flask.route('/logout', methods=['DELETE'])
@jwt_required
def logout_user():
    '''Handle DELETE request to log a user out. The JWT token is revoked by 
    adding it it to a blacklist.\n
    :return message in JSON form'''
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return {"message": f"Successfully logged out {get_jwt_identity()}"}

@app_flask.route('/user/<username>/', methods=['DELETE'])
@app_flask.route('/user/<username>', methods=['DELETE'])
@jwt_required
def delete_user(username):
    '''Handle DELETE request to delete a user account.\n
    :param username : username of user to be deleted.
    '''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        user = User.query.filter_by(username=username).first()
        if user is None:
            return {"error": "Resource not found"}, 404
        if current_user is user:
            current_user.is_active = False
            db.session.commit()
            logout_user()
            return {"message": f"Successfully deleted user {username}."}
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500


@app_flask.route('/tags/')
@app_flask.route('/tags')
def get_tags():
    '''Handles GET request to view all tags.\n
    :return response : JSON form of all tags.
    '''
    try:
        tags = Tag.query.all()
        response = [tag.convert_to_json_form() for tag in tags]
        return jsonify(response)
    except:
        save_error_log(error_log_file)
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/')
@app_flask.route('/questions')
def get_all_questions():
    '''Handles GET request to view all questions on the board.\n
    If tags are given as query parameter, questions with those tags are
    displayed.\n
    :return response : JSON from of all questions on the board.'''
    try:
        question_set = set(Question.query.all())
        tagstring = request.args.get('tags')
        if tagstring:
            for each_tag in tagstring.split(','):
                tag = Tag.query.filter_by(name=each_tag).first()
                if tag is None:
                    return jsonify([])
                qtags = tag.qtags
                tag_questions = set(Question.query.get(qtag.question_id)\
                    for qtag in qtags)
                question_set = question_set.intersection(tag_questions)
        response = [question.convert_to_json_form()\
            for question in question_set]
        return jsonify(response)
    except:
        save_error_log(error_log_file)
        return {"error": "Something went wrong at our end"}, 500


@app_flask.route('/questions/<question_id>/')
@app_flask.route('/questions/<question_id>')
def display_question(question_id):
    '''Handles GET request to view a specific question on the board.\n
    :param question_id: str, id of question to be viewed.\n
    :return question : JSON form of question, along with answers.
    '''
    try:
        question = Question.query.get(question_id)
        if question is None:
            return {"error": "Resource Not found"}, 404
        return question.convert_to_json_form()
    except:
        save_error_log(error_log_file)
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/answers/')
@app_flask.route('/questions/<question_id>/answers')
def get_answers(question_id):
    '''Handles GET request to view answers to a specific question.\n
    :param question_id : string, id of question to be viewed.\n
    :return response : JSON form of answers to a question.
    '''
    try:
        question = Question.query.get(question_id)
        if question is None:
            return {"error": "Resource Not found"}, 404
        answers = question.answers
        response = [answer.convert_to_json_form() for answer in answers]
        return jsonify(response)
    except:
        save_error_log(error_log_file)
        return {"error": "Something went wrong at our end"}, 500


@app_flask.route('/questions/<question_id>/answers/<answer_id>/')
@app_flask.route('/questions/<question_id>/answers/<answer_id>')
def get_one_answer(question_id, answer_id):
    '''Handles GET request to view one particular answer, with the answer id.\n
    :param question_id : str question id\n
    :param answer_id : str answer id\n
    :return answer : JSON form.
    '''
    try:
        question = Question.query.get(question_id)
        answer = Answer.query.get(answer_id)
        if question is None or answer is None:
            return {"error": "Resource Not found"}, 404
        return answer.convert_to_json_form()
    except:
        save_error_log(error_log_file)
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/user/<user_id>/activity/')
@app_flask.route('/user/<user_id>/activity')
def view_user(user_id):
    '''Handles GET request to view all activity by a user.\n
    :param user_id: str, id of the user.\n
    :return response : dict, questions and answers by user.
    '''
    try:
        user = User.query.get(user_id)
        if user is None or not user.is_active:
            return {"error": "Resource not found"}, 404
        response = {}
        response['questions'] = [question.convert_to_json_form()\
            for question in user.questions]
        response['answers'] = [answer.convert_to_json_form()\
            for answer in user.answers]
        return response
    except:
        save_error_log(error_log_file)
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/', methods=['POST'])
@app_flask.route('/questions', methods=['POST'])
@jwt_required
def post_question():
    '''Handles POST request to ask a new question to the board.\n
    :return redirect : link to added question.'''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        if not current_user.is_active:
            return {"error": "Unauthorised access"}, 401
        request_body = request.get_json()
        title = request_body['title']
        description = request_body['description']
        question = Question(title=title, description=description,\
            user_id=current_user.id)
        tags = find_tags_for(request_body)
        db.session.add(question)
        db.session.flush()
        add_tags_to_db(question, tags)
        db.session.commit()
        return redirect(f'/questions/{question.id}')
    except (KeyError, werkzeug.exceptions.BadRequest):
        db.session.rollback()
        return {"error": "Bad Request"}, 400
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/answers/', methods=['POST'])
@app_flask.route('/questions/<question_id>/answers', methods=['POST'])
@jwt_required
def post_answer(question_id):
    '''Handles POST request to answer a question.\n
    :param question_id : id of question which is to be answered.\n
    :return redirect : the posted answer.
    '''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        if not current_user.is_active:
            return {"error": "Unauthorised access"}, 401
        question = Question.query.get(question_id)
        answer_body = request.get_json()['body']
        if question is None:
            return {"error": "Resource Not found"}, 404
        if answer_body:
            answer = Answer(question_id=question_id, body=answer_body,\
                user_id=current_user.id)
            db.session.add(answer)
            db.session.commit()
        return redirect(f'/questions/{question_id}/answers/{answer.id}')
    except (KeyError, werkzeug.exceptions.BadRequest):
        db.session.rollback()
        return {"error": "Bad Request"}, 400
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/', methods=['PUT'])
@app_flask.route('/questions/<question_id>', methods=['PUT'])
@jwt_required
def edit_question(question_id):
    '''Handles PUT request to edit a question.\n
    :param question_id : string, id of question to be edited.\n
    :return redirect : link to edited question'''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        question = Question.query.get(question_id)
        if question is None:
            return {"error": "Resource Not found"}, 404
        if question.author is current_user and current_user.is_active:
            request_body = request.get_json()
            old_qtags = set(question.qtags)
            for qtag in old_qtags:
                db.session.delete(qtag)
            new_tags = set(find_tags_for(request_body)) - old_qtags
            question.title = request_body['title']
            question.description = request_body['description']
            add_tags_to_db(question, new_tags)
            db.session.commit()
            return redirect(f'/questions/{question_id}')
        return {'error': 'Unauthorised access'}, 401
    except (KeyError, werkzeug.exceptions.BadRequest):
        db.session.rollback()
        return {"error": "Bad Request"}, 400
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/answers/<answer_id>/',\
    methods=['PUT'])
@app_flask.route('/questions/<question_id>/answers/<answer_id>',\
    methods=['PUT'])
@jwt_required
def edit_answer(question_id, answer_id):
    '''Handles PUT request to edit an answer.\n
    :param question_id : id of question\n
    :param answer_id : id of answer to be edited.\n
    :return redirect : link to edited answer.'''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        question = Question.query.get(question_id)
        answer = Answer.query.get(answer_id)
        if question is None or answer is None:
            return {"error": "Resource Not Found"}, 404
        if answer.author is current_user and current_user.is_active:
            answer.body = request.get_json()['body']
            db.session.commit()
            return redirect(f'/questions/{question_id}/answers/{answer_id}/')
        return {"error": "Unauthorised Access"}, 401
    except (KeyError, werkzeug.exceptions.BadRequest):
        db.session.rollback()
        return {"error": "Bad Request"}, 400
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/<vote>/', methods=['PUT'])
@app_flask.route('/questions/<question_id>/<vote>', methods=['PUT'])
@jwt_required
def vote_question(question_id, vote):
    '''Handles PUT request to vote on a question.
    :param question_id : id of question\n
    :param vote : string, one of ['upvote', 'downvote']\n
    :return redirect : link to question
    '''
    try:
        question = Question.query.get(question_id)
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        if not current_user.is_active:
            return {"error": "Unauthorised access"}, 401
        if question is None:
            return {"error": "Resource Not found"}, 404
        if vote == 'upvote':
            question.score += 1
        elif vote == 'downvote':
            question.score -= 1
        else:
            return {'error': 'Bad Request'}, 400
        db.session.commit()
        return redirect(f'/questions/{question_id}')
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/answers/<answer_id>/<vote>/',\
    methods=['PUT'])
@app_flask.route('/questions/<question_id>/answers/<answer_id>/<vote>',\
    methods=['PUT'])
@jwt_required
def vote_answer(question_id, answer_id, vote):
    '''Handles PUT request to vote on an answer.
    :param question_id : id of question\n
    :param answer_id : id of answer\n
    :param vote : string, one of ['upvote', 'downvote']\n
    :return redirect : link to answer
    '''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        if not current_user.is_active:
            return {"error": "Unauthorised access"}, 401
        question = Question.query.get(question_id)
        answer = Answer.query.get(answer_id)
        if question is None or answer is None:
            return {"error": "Resource Not found"}, 404
        if vote == 'upvote':
            answer.score += 1
        elif vote == 'downvote':
            answer.score -= 1
        else:
            return {'error': 'Bad Request'}, 400
        db.session.commit()
        return redirect(f'/questions/{question_id}/answers/{answer_id}/')
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/', methods=['DELETE'])
@app_flask.route('/questions/<question_id>', methods=['DELETE'])
@jwt_required
def delete_question(question_id):
    '''Handles DELETE request on a question.\n
    :param question_id : id of quesiton to be deleted.\n
    :return empty list'''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        question = Question.query.get(question_id)
        if question is None:
            return {"error": "Resource Not found"}, 404
        if question.author is current_user and current_user.is_active:
            answers = list(question.answers)
            qtags = list(question.qtags)
            delete_list = [question] + answers + qtags
            for entry in delete_list:
                db.session.delete(entry)
            db.session.commit()
            return {"message": "Deleted question."}
        return {'error': 'Unauthorised access'}, 401
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

@app_flask.route('/questions/<question_id>/answers/<answer_id>/',\
    methods=['DELETE'])
@app_flask.route('/questions/<question_id>/answers/<answer_id>',\
    methods=['DELETE'])
@jwt_required
def delete_answer(question_id, answer_id):
    '''Handles DELETE request on an answer.\n
    :param question_id : str, question id\n
    :param answer_id : str, answer id\n
    :return redirect : link to question.
    '''
    try:
        current_user = User.query.filter_by(username=get_jwt_identity()).first()
        question = Question.query.get(question_id)
        answer = Answer.query.get(answer_id)
        if question is None or answer is None:
            return {"error": "Resource Not found"}, 404
        if answer.author is current_user and current_user.is_active:
            db.session.delete(answer)
            db.session.commit()
            return {"message": "Deleted answer."}
        return {"error": "Unauthorised Access"}, 401
    except:
        save_error_log(error_log_file)
        db.session.rollback()
        return {"error": "Something went wrong at our end"}, 500

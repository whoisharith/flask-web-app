'''Contains helper functions'''

import re
from datetime import datetime
import traceback

from app import db
from app.models import Tag, QuestionTag


def englify(word):
    '''Remove non alpha-numeric characters from a word.\n
    :param word : str, word to be cleaned.\n
    :return : str, word with only alpha-numeric characters.
    '''
    return re.sub(r'[^a-zA-Z0-9]', '', word).lower()

def find_tags_for(request_body):
    '''Find tags for the posted/edited question.\n
    If tags are sent as part of the request body, they are used.\n
    Else, tags are automatically chosen from the largest 5 words of the
    title and description.\n
    :param request_body : JSON, request body\n
    :return tags : list of str tags.
    '''
    title = request_body['title']
    description = request_body['description']
    words = set(map(englify, title.split() + description.split()))
    try:
        tags = request_body['tags']
        tag_0 = tags[0]
        del tag_0
    except (KeyError, IndexError):
        # No tags sent as part of the request body.
        tags = sorted(words, key=len)[-5:]
    return tags

def add_tags_to_db(question, tags):
    '''Add tags to the database.\n
    :param question: Question object (row from the question table in the db).\n
    :param tags : list of str tags.
    '''
    for tag_name in tags:
        tag_existing = Tag.query.filter_by(name=tag_name).first()
        if not tag_existing and tag_name:
            tag = Tag(name=tag_name)
            db.session.add(tag)
            db.session.flush()
        else:
            tag = tag_existing
        qtag = QuestionTag(question_id=question.id, tag_id=tag.id)
        db.session.add(qtag)

def save_error_log(log_file):
    '''Save a traceback to a file, in case of an unexpected error.'''
    error_log = open(log_file, 'a')
    time_now = datetime.utcnow()
    error = f'Error\nTime: {time_now.strftime("%Y/%m/%d, %H:%M:%S")} UTC\n'\
        + f'\n{traceback.format_exc()}\n'
    error_log.write(error)
    error_log.close()
